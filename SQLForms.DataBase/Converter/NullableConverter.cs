﻿namespace SQLForms.DataBase.Converter;

class NullableConverter : IValueConverter
{
    List<IValueConverter> valueConverters = new List<IValueConverter>()
    {
        new Int32Converter(),
        new DateTimeConverter(),
        new BooleanConverter(),
        new DoubleConverter()
    };

    public bool CanConvert(Type targetType)
    {
        if (!targetType.IsGenericType || targetType.GetGenericTypeDefinition() != typeof(Nullable<>))
        {
            return false;
        }

        var underlyingType = Nullable.GetUnderlyingType(targetType);
        if (underlyingType == null)
        {
            return false;
        }

        return valueConverters.Any(q => q.CanConvert(underlyingType));
    }

    public object Convert(object value, Type targetType)
    {
        if (value == null)
        {
            return null;
        }

        if (!CanConvert(targetType))
        {
            throw new InvalidOperationException($"Cannot convert value of type '{targetType}' to nullable.");
        }

        var underlyingType = Nullable.GetUnderlyingType(targetType);
        var converter = valueConverters.FirstOrDefault(q => q.CanConvert(underlyingType));
        return converter?.Convert(value, underlyingType);
    }
}