﻿namespace SQLForms.DataBase.Converter;

internal class DoubleConverter : AbstractValueConverter<double>
{
    public override double Convert(object value) => System.Convert.ToDouble(value);
}
