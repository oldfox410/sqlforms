﻿namespace SQLForms.DataBase.Converter;

internal class StringConverter : AbstractValueConverter<string>
{
    public override string Convert(object value) => System.Convert.ToString(value) ?? string.Empty;
}
