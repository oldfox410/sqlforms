﻿namespace SQLForms.DataBase.Converter;

internal abstract class AbstractValueConverter<T> : IValueConverter<T>
{
    public bool CanConvert(Type targetType)
    {
        return targetType == typeof(T);
    }

    public object Convert(object value, Type targetType)
    {
        if (targetType != typeof(T))
            throw new InvalidOperationException("Can't convert to type " + targetType.Name);

        return Convert(value);
    }

    public abstract T Convert(object value);
}
