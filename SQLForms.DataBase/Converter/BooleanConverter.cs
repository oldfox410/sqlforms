﻿namespace SQLForms.DataBase.Converter;

internal class BooleanConverter : AbstractValueConverter<bool>
{
    public override bool Convert(object value) => System.Convert.ToBoolean(value);
}
