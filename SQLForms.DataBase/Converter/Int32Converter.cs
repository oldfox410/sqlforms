﻿namespace SQLForms.DataBase.Converter;

internal class Int32Converter : AbstractValueConverter<int>
{
    public override int Convert(object value) => System.Convert.ToInt32(value);
}
