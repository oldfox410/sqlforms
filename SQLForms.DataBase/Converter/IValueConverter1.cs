﻿namespace SQLForms.DataBase.Converter;

interface IValueConverter
{
    bool CanConvert(Type targetType);

    object Convert(object value, Type targetType);
}
