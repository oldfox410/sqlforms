﻿namespace SQLForms.DataBase.Converter;

interface IValueConverter<T> : IValueConverter
{
    T Convert(object value);
}
