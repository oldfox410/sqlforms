﻿namespace SQLForms.DataBase.Converter;

internal class DateTimeConverter : AbstractValueConverter<DateTime>
{
    public override DateTime Convert(object value) => System.Convert.ToDateTime(value);
}
