﻿using System.Data.SqlClient;
using System.Reflection;
using SQLForms.DataBase.Converter;
using SQLForms.Domain.Interfaces;

namespace SQLForms.DataBase;

/// <summary>
/// Фасад CRUD-операций над DB.
/// </summary>
public static class DbFacade
{
    /// <summary>
    /// Строка для авторизации в SQL Server.
    /// </summary>
    public static string ConnectionString => "Server=localhost;Database=universitydb;User Id=SQLFormsApp;Password=development;";

    /// <summary>
    /// Стретегии конвертации принимаемых из таблиц значений.
    /// </summary>
    static List<IValueConverter> _valueConverters = new List<IValueConverter>()
    {
        new NullableConverter(),
        new Int32Converter(),
        new BooleanConverter(),
        new DoubleConverter(),
        new StringConverter(),
        new DateTimeConverter(),
    };

    public static string? Shielding(string? str)
    {
        return str?.Replace("'", "''");
    }

    /// <summary>
    /// Создание строки в таблице с помощью INSERT-скрипта.
    /// </summary>
    /// <typeparam name="T">Тип сущности.</typeparam>
    /// <param name="item">Сущность.</param>
    /// <returns>Id созданной сущности.</returns>
    public static int Create<T>(T item)
        where T : IEntity, new()
    {
        var type = typeof(T);
        var columns = type.GetProperties().Where(q => q.CanWrite);
        var data = new List<string>();
        var fields = new List<string>();
        foreach (var column in columns)
        {
            if (column.Name == "Id")
                continue;
            fields.Add(column.Name);
            object? value = column.GetValue(item);
            if (value == null)
                data.Add("NULL");
            else if (value.GetType() == typeof(string) || value.GetType() == typeof(DateTime))
                data.Add("'" + Shielding(Convert.ToString(value)) + "'");
            else
#pragma warning disable CS8604 // Возможно, аргумент-ссылка, допускающий значение NULL. Проверка на null производится ранее.
                data.Add(Convert.ToString(value));
#pragma warning restore CS8604 // Возможно, аргумент-ссылка, допускающий значение NULL.
        }
        var queryString = "INSERT INTO " + type.Name + "(" + fields.Aggregate((acc, curr) => acc += ", " + curr) + ") VALUES (" + data.Aggregate((acc, curr) => acc += ", " + curr) + ")";
        
        using var dataSource = new SqlConnection(ConnectionString);
        dataSource.Open();
        SqlCommand command = new SqlCommand(queryString, dataSource);
        
        var result = command.ExecuteScalar();
        dataSource.Close();
        return Convert.ToInt32(result);
    }

    /// <summary>
    /// Чтение данных посредством Read, по умолчанию ищет таблицу с именем сущности, в иных случаях следует писать скрипт от руки.<br/>
    /// Принимает SQL-запрос, Небезопасная штука, может сделать что угодно от имени программы. Так как делалось "на скорую руку", это пока что самый простой вариант сделать WHERE.<br/>
    /// Однозначно не рекомендуется принимать в эту строку что угодно введённое пользователем.<br/>
    /// А также называть своего сына Robert '); DROP TABLE Students;-- https://xkcd.ru/327/
    /// </summary>
    /// <typeparam name="T">Тип возвращаемой сущности.</typeparam>
    /// <param name="sqlScript">SELECT-SQL запрос.</param>
    /// <returns></returns>
    public static List<T> Read<T>(string? sqlScript = null)
        where T : IEntity, new()
    {
        using var dataSource = new SqlConnection(ConnectionString);
        dataSource.Open();
        
        // Стандартный SELECT-скрипт.
        sqlScript ??= "SELECT * FROM " + typeof(T).Name;

        var command = new SqlCommand(sqlScript, dataSource);
        var reader = command.ExecuteReader();
        List<T> result = new List<T>();
        while (reader.Read())
        {
            var item = new T();
            var columns = typeof(T).GetProperties();
            foreach (var column in columns)
            {
                var data = reader[column.Name];
                column.SetValue(item, _valueConverters.FirstOrDefault(q => q.CanConvert(column.PropertyType)).Convert(data, column.PropertyType));
            }
            result.Add(item);
        }

        dataSource.Close();

        return result;
    }

    /// <summary>
    /// Обновление сущности с помощью UPDATE-скрипта. Работает только с одной сущностью.
    /// </summary>
    /// <typeparam name="T">Тип сущности, по нему определяется таблица.</typeparam>
    /// <param name="entity">Изменённая сущность</param>
    /// <returns>true, если измениие успешно, иначе false</returns>
    public static int Update<T>(T entity) 
        where T : IEntity, new()
    {
        var type = typeof(T);
        var columns = type.GetProperties().Where(q => q.CanWrite);
        string queryString = "UPDATE " + type.Name + " SET ";
        var enumerator = columns.GetEnumerator();
        enumerator.MoveNext();
        var column = enumerator.Current;

        Func<PropertyInfo?, string?> mapToObject = (c) =>
        {
            if (c == null)
                return null;

            if (c.Name == "Id")
                return null;

            object? value = c.GetValue(entity);

            string valueToString;

            if (value == null)
                valueToString = "NULL";
            else if (value.GetType() == typeof(string) || value.GetType() == typeof(DateTime))
                valueToString = "'" + Shielding(Convert.ToString(value)) + "'";
            else
#pragma warning disable CS8600 // Преобразование литерала, допускающего значение NULL или возможного значения NULL в тип, не допускающий значение NULL.
                // Проверка на NULL производится ранее.
                valueToString = Convert.ToString(value);
#pragma warning restore CS8600 // Преобразование литерала, допускающего значение NULL или возможного значения NULL в тип, не допускающий значение NULL.
            return c.Name + " = " + valueToString;
        };

        queryString += mapToObject(column);

        while (enumerator.MoveNext())
        {
            column = enumerator.Current;

            var value = mapToObject(column);

            if (value == null)
                continue;

            queryString += ", " + value;
        }
        queryString += " WHERE Id = " + entity.Id;

        using var dataSource = new SqlConnection(ConnectionString);
        dataSource.Open();
        SqlCommand command = new SqlCommand(queryString, dataSource);

        var result = command.ExecuteNonQuery();
        dataSource.Close();
        return result;
    }

    /// <summary>
    /// Удаление записи.
    /// </summary>
    /// <typeparam name="T">Тип удаляемой сущности.</typeparam>
    /// <param name="entity">Сущность.</param>
    /// <returns>true, если операция выполнена успешно, иначе false</returns>
    public static int Remove<T>(T entity)
        where T : IEntity, new()
    {
        if(entity.Id == null)
            throw new ArgumentNullException(nameof(entity.Id));

        return Remove<T>(entity.Id.Value);
    }

    public static int Remove<T>(int id)
        where T : IEntity, new()
    {
        using var dataSource = new SqlConnection(ConnectionString);
        dataSource.Open();
        SqlCommand command = new SqlCommand("DELETE FROM " + typeof(T).Name + " WHERE Id = " + id, dataSource);

        var result = command.ExecuteNonQuery();
        dataSource.Close();
        return result;
    }
}
