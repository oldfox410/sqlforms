USE institutedb

-- Создание таблицы "Speciality"
CREATE TABLE Speciality (
    Id INT IDENTITY(1,1) PRIMARY KEY,
    Name VARCHAR(128) NULL
);

-- Создание таблицы "Student"
CREATE TABLE Student (
    Id INT IDENTITY(1,1) PRIMARY KEY,
    FullName VARCHAR(128) NULL,
    RecordBookNumber INT NOT NULL,
    BirthDate DATE NOT NULL,
    SpecialityId INT NULL,
    GroupName VARCHAR(128) NULL,
    City VARCHAR(128) NULL,
    Address VARCHAR(128) NULL,
    FOREIGN KEY (SpecialityId) REFERENCES Speciality(Id)
);

-- Создание таблицы "Exam"
CREATE TABLE Exam (
    Id INT IDENTITY(1,1) PRIMARY KEY,
    StudentId INT NULL,
    Grade INT NOT NULL,
    ExamDate DATE NOT NULL,
    FOREIGN KEY (StudentId) REFERENCES Student(Id)
);