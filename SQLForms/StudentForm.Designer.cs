﻿namespace SQLForms;

partial class StudentForm
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.add = new System.Windows.Forms.Button();
            this.preview = new System.Windows.Forms.Button();
            this.address = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.city = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.group = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.specialtyNumber = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.birthDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.fullName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.recordBookNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // add
            // 
            this.add.Location = new System.Drawing.Point(12, 247);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(184, 29);
            this.add.TabIndex = 45;
            this.add.Text = "Применить";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.OnAddClick);
            //
            // pereview
            //
            this.preview.Location = new System.Drawing.Point(202, 247);
            this.preview.Name = "preview";
            this.preview.Size = new System.Drawing.Size(184, 29);
            this.preview.TabIndex = 45;
            this.preview.Text = "Предпросмотр";
            this.preview.UseVisualStyleBackColor = true;
            this.preview.Click += new System.EventHandler(this.OnPreviewClick);
            // 
            // address
            // 
            this.address.Location = new System.Drawing.Point(12, 191);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(184, 27);
            this.address.TabIndex = 44;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 20);
            this.label7.TabIndex = 43;
            this.label7.Text = "Адрес:";
            // 
            // city
            // 
            this.city.Location = new System.Drawing.Point(202, 139);
            this.city.Name = "city";
            this.city.Size = new System.Drawing.Size(184, 27);
            this.city.TabIndex = 42;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(202, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 20);
            this.label6.TabIndex = 41;
            this.label6.Text = "Город:";
            // 
            // group
            // 
            this.group.Location = new System.Drawing.Point(202, 86);
            this.group.Name = "group";
            this.group.Size = new System.Drawing.Size(184, 27);
            this.group.TabIndex = 40;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(202, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 20);
            this.label5.TabIndex = 39;
            this.label5.Text = "Группа:";
            // 
            // specialtyNumber
            // 
            this.specialtyNumber.FormattingEnabled = true;
            this.specialtyNumber.Location = new System.Drawing.Point(202, 32);
            this.specialtyNumber.Name = "specialtyNumber";
            this.specialtyNumber.Size = new System.Drawing.Size(184, 28);
            this.specialtyNumber.TabIndex = 38;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(202, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 20);
            this.label4.TabIndex = 37;
            this.label4.Text = "Специальность:";
            // 
            // birthDate
            // 
            this.birthDate.Location = new System.Drawing.Point(12, 138);
            this.birthDate.Name = "birthDate";
            this.birthDate.Size = new System.Drawing.Size(184, 27);
            this.birthDate.TabIndex = 36;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 20);
            this.label3.TabIndex = 35;
            this.label3.Text = "Дата рождения:";
            // 
            // fullName
            // 
            this.fullName.Location = new System.Drawing.Point(12, 85);
            this.fullName.Name = "fullName";
            this.fullName.Size = new System.Drawing.Size(184, 27);
            this.fullName.TabIndex = 34;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 20);
            this.label2.TabIndex = 33;
            this.label2.Text = "ФИО:";
            // 
            // recordBookNumber
            // 
            this.recordBookNumber.Location = new System.Drawing.Point(12, 32);
            this.recordBookNumber.Name = "recordBookNumber";
            this.recordBookNumber.Size = new System.Drawing.Size(184, 27);
            this.recordBookNumber.TabIndex = 32;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 20);
            this.label1.TabIndex = 31;
            this.label1.Text = "Номер зачётной книжки:";
            // 
            // StudentForm
            // 
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 286);
            this.Controls.Add(this.add);
            this.Controls.Add(this.preview);
            this.Controls.Add(this.address);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.city);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.group);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.specialtyNumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.birthDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fullName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.recordBookNumber);
            this.Controls.Add(this.label1);
            this.Name = "StudentForm";
            this.Text = "StudentForm";
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private Button add;
    private Button preview;
    private TextBox address;
    private Label label7;
    private TextBox city;
    private Label label6;
    private TextBox group;
    private Label label5;
    private ComboBox specialtyNumber;
    private Label label4;
    private DateTimePicker birthDate;
    private Label label3;
    private TextBox fullName;
    private Label label2;
    private TextBox recordBookNumber;
    private Label label1;
}