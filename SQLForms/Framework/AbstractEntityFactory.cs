﻿using SQLForms.DataBase;
using SQLForms.Domain.Interfaces;
using SQLForms.Interfaces;

namespace SQLForms.Framework;

public abstract class AbstractEntityFactory<T> : IAbstractFactory
    where T : IEntity, new()
{
    public bool CanCreate(Type entityType)
    {
        return entityType == typeof(T);
    }

    public abstract IEntity CreateEntity();

    public abstract IEntityForm? CreateForm();

    public IEnumerable<IEntity> GetEntities(string? query = null) => DbFacade.Read<T>(query).Select(q => (IEntity)q);
}