﻿using SQLForms.Domain.Contract;
using SQLForms.Domain.Interfaces;

namespace SQLForms.Framework;

public class StudentFactory : AbstractEntityFactory<Student>
{
    public override IEntity CreateEntity() => new Student();

    public override StudentForm CreateForm() => new StudentForm();
}
