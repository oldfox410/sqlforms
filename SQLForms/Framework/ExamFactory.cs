﻿using SQLForms.Domain.Contract;
using SQLForms.Domain.Interfaces;

namespace SQLForms.Framework;

public class ExamFactory : AbstractEntityFactory<Exam>
{
    public override ExamForm CreateForm() => new ExamForm();

    public override IEntity CreateEntity() => new Exam();
}