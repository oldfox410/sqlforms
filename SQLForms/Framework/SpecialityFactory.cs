﻿using SQLForms.Domain.Contract;
using SQLForms.Domain.Interfaces;
using SQLForms.Interfaces;

namespace SQLForms.Framework;

public class SpecialityFactory : AbstractEntityFactory<Speciality>
{
    public override IEntity CreateEntity() => new Speciality();

    public override IEntityForm? CreateForm() => null;
}