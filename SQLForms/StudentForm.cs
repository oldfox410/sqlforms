﻿using SQLForms.DataBase;
using SQLForms.Domain.Contract;


namespace SQLForms;
public partial class StudentForm : EntityForm<Student>
{
    List<Speciality> specialities;

#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
    // Производится внутри InitSpeialties.
    public StudentForm()
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
    {
        InitializeComponent();
        InitSpecialties();
    }

    public StudentForm(int id) : this()
    {
        EntityId = id;
    }

    public override void ViewEntity()
    {
        address.Text = Entity.Address;
        birthDate.Text = Entity.BirthDate.ToString();
        city.Text = Entity.City;
        specialtyNumber.SelectedIndex = Entity.SpecialityId.GetValueOrDefault();
        fullName.Text = Entity.FullName;
        group.Text = Entity.GroupName;
        recordBookNumber.Text = Entity.RecordBookNumber.ToString();
    }

    void InitSpecialties()
    {
        specialities = DbFacade.Read<Speciality>();
        foreach (var speciality in specialities)
        {
            specialtyNumber.Items.Add(speciality.Name);
        }
    }

    public override bool Validation()
    {
        if (specialtyNumber.SelectedIndex < 0 || specialtyNumber.SelectedIndex >= specialities.Count())
        {
            MessageBox.Show("Поле \"Специальность\" заполнено некорректно");
            return false;
        }
        return true;
    }

    public override Student MapEntity()
    {
        return new Student()
        {
            Id = EntityId,
            Address = address.Text,
            BirthDate = Convert.ToDateTime(birthDate.Text),
            City = city.Text,
            SpecialityId = Convert.ToInt32(specialities[specialtyNumber.SelectedIndex - 1].Id),
            FullName = fullName.Text,
            GroupName = group.Text,
            RecordBookNumber = Convert.ToInt32(recordBookNumber.Text)
        };
    }
}
