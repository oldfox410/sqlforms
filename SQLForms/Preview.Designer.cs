﻿namespace SQLForms;

partial class Preview<T>
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.dataView = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // dataView
            // 
            this.dataView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataView.FormattingEnabled = true;
            this.dataView.ItemHeight = 20;
            this.dataView.Location = new System.Drawing.Point(11, 12);
            this.dataView.Name = "dataView";
            this.dataView.Size = new System.Drawing.Size(775, 264);
            this.dataView.TabIndex = 1;
            // 
            // Preview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 288);
            this.Controls.Add(this.dataView);
            this.Name = "Preview";
            this.Text = "Preview";
            this.ResumeLayout(false);

    }

    #endregion

    private ListBox dataView;
}