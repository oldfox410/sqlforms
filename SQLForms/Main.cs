namespace SQLForms;

public partial class Main : Form
{
    public Main()
    {
        InitializeComponent();
    }

    private void view_Click(object sender, EventArgs e)
    {
        new View().ShowDialog();
    }

    private void search_Click(object sender, EventArgs e)
    {
        new Search().ShowDialog();
    }

    private void add_Click(object sender, EventArgs e)
    {
        new StudentForm().ShowDialog();
    }

    private void button1_Click(object sender, EventArgs e)
    {
        new ExamForm().ShowDialog();
    }
}
