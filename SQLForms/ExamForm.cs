﻿using SQLForms.DataBase;
using SQLForms.Domain.Contract;

namespace SQLForms;
public partial class ExamForm : EntityForm<Exam>
{
    List<Student> students;

#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
    // Заполнение происходит внутри InitStudents.
    public ExamForm()
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
    {
        InitializeComponent();
        InitStudents();
    }

    public override void ViewEntity()
    {
        studentId.SelectedIndex = Entity.StudentId.GetValueOrDefault();
        examDate.Text = Entity.ExamDate.ToString();
        grade.Text = Entity.Grade.ToString();
    }

    public ExamForm(int id) : this()
    {
        EntityId = id;
    }

    void InitStudents()
    {
        students = DbFacade.Read<Student>();
        foreach (var student in students)
        {
            studentId.Items.Add(student.FullName);
        }
    }

    public override bool Validation()
    {
        if (studentId.SelectedIndex < 0 || studentId.SelectedIndex >= students.Count())
        {
            MessageBox.Show("Поле \"Студент\" заполнено некорректно");
            return false;
        }
        return true;
    }

    public override Exam MapEntity() => new Exam()
    {
        Id = EntityId,
        StudentId = students[studentId.SelectedIndex].Id,
        ExamDate = Convert.ToDateTime(examDate.Text),
        Grade = Convert.ToInt32(grade.Text)
    };
}
