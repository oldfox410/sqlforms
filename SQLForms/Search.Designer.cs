﻿namespace SQLForms;

partial class Search
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.dataSearch = new System.Windows.Forms.ListBox();
            this.isFullName = new System.Windows.Forms.CheckBox();
            this.fullNameFilter = new System.Windows.Forms.TextBox();
            this.groupFilter = new System.Windows.Forms.TextBox();
            this.isGroup = new System.Windows.Forms.CheckBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dataView
            // 
            this.dataSearch.FormattingEnabled = true;
            this.dataSearch.ItemHeight = 20;
            this.dataSearch.Location = new System.Drawing.Point(12, 54);
            this.dataSearch.Name = "dataView";
            this.dataSearch.Size = new System.Drawing.Size(776, 384);
            this.dataSearch.TabIndex = 1;
            // 
            // isFullName
            // 
            this.isFullName.AutoSize = true;
            this.isFullName.Location = new System.Drawing.Point(12, 12);
            this.isFullName.Name = "isFullName";
            this.isFullName.Size = new System.Drawing.Size(67, 24);
            this.isFullName.TabIndex = 2;
            this.isFullName.Text = "ФИО:";
            this.isFullName.UseVisualStyleBackColor = true;
            // 
            // fullNameFilter
            // 
            this.fullNameFilter.Location = new System.Drawing.Point(85, 10);
            this.fullNameFilter.Name = "fullNameFilter";
            this.fullNameFilter.Size = new System.Drawing.Size(154, 27);
            this.fullNameFilter.TabIndex = 3;
            // 
            // groupFilter
            // 
            this.groupFilter.Location = new System.Drawing.Point(334, 10);
            this.groupFilter.Name = "groupFilter";
            this.groupFilter.Size = new System.Drawing.Size(154, 27);
            this.groupFilter.TabIndex = 5;
            // 
            // isGroup
            // 
            this.isGroup.AutoSize = true;
            this.isGroup.Location = new System.Drawing.Point(245, 12);
            this.isGroup.Name = "isGroup";
            this.isGroup.Size = new System.Drawing.Size(83, 24);
            this.isGroup.TabIndex = 4;
            this.isGroup.Text = "Группа:";
            this.isGroup.UseVisualStyleBackColor = true;
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(694, 10);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(94, 29);
            this.searchButton.TabIndex = 6;
            this.searchButton.Text = "Поиск";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.groupFilter);
            this.Controls.Add(this.isGroup);
            this.Controls.Add(this.fullNameFilter);
            this.Controls.Add(this.isFullName);
            this.Controls.Add(this.dataSearch);
            this.Name = "Search";
            this.Text = "Search";
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private ListBox dataSearch;
    private CheckBox isFullName;
    private TextBox fullNameFilter;
    private TextBox groupFilter;
    private CheckBox isGroup;
    private Button searchButton;
}