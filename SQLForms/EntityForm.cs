﻿using SQLForms.DataBase;
using SQLForms.Domain.Contract;
using SQLForms.Interfaces;

namespace SQLForms;

public abstract class EntityForm<T> : Form, IEntityForm
    where T : BaseEntity, new()
{

    /// <summary>
    /// Id существующей сущности или null.
    /// </summary>
    public virtual int? EntityId { get; set; }


    private T? _entity;

    /// <summary>
    /// Сущность на экране.
    /// </summary>
    public virtual T Entity
    {
        get
        {
            if(_entity != null)
                return _entity;

            if (EntityId == null)
                return _entity = new T();

            _entity ??= DbFacade.Read<T>("SELECT * FROM " + typeof(T).Name + " WHERE Id = " + EntityId).FirstOrDefault() ?? new T();

            return _entity;
        }
    }

    /// <summary>
    /// Валидация для конкретной реализации.
    /// </summary>
    /// <returns></returns>
    public virtual bool Validation()
    {
        return true;
    }

    /// <summary>
    /// Получение сущности из полученных через форму данных.
    /// </summary>
    /// <returns></returns>
    public abstract T MapEntity();

    /// <summary>
    /// Отображение сущности, если она уже существует.
    /// </summary>
    public abstract void ViewEntity();

    /// <summary>
    /// Метод для ивента нажатия кнопки "Добавить"
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public virtual void OnAddClick(object sender, EventArgs e)
    {
        if (!Validation())
            return;
        AddOrUpdate(MapEntity());
    }

    /// <summary>
    /// Метод для ивента нажатия кнопки "Предпросмотр"
    /// </summary>
    public virtual void OnPreviewClick(object sender, EventArgs e)
    {
        if (!Validation())
            return;
        PreviewEntity(MapEntity());
    }

    /// <summary>
    /// Метод для ивента нажатия кнопки "Удалить"
    /// </summary>
    public virtual void OnDeleteClick(object sender, EventArgs e)
    {
        if(EntityId == null)
        {
            MessageBox.Show("Невозможно удалить несуществующую запись", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        DeleteEntity(MapEntity());
    }

    /// <summary>
    /// Предпросмотр сущности.
    /// </summary>
    /// <param name="entity"></param>
    public void PreviewEntity(T entity)
    {
        new Preview<T>(entity).ShowDialog();
    }

    /// <summary>
    /// Обновление или добавление сущности в зависимости от наличия Id.
    /// </summary>
    /// <param name="entity"></param>
    public void AddOrUpdate(T entity)
    {
        if (entity.Id == null)
            AddEntity(entity);
        else
            UpdateEntity(entity);
        this.Close();
    }

    /// <summary>
    /// Добавление сущности.
    /// </summary>
    /// <param name="entity"></param>
    public void AddEntity(T entity)
    {
        try
        {
            var result = DbFacade.Create(entity);

            if (EntityId == null)
                Text += " (изменение)";
            EntityId = result;
            MessageBox.Show("Сущность успешно добавлена!", "Info.", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        catch (Exception ex)
        {
            MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }

    /// <summary>
    /// Обновление сущности.
    /// </summary>
    /// <param name="entity"></param>
    public void UpdateEntity(T entity)
    {
        try
        {
            var id = DbFacade.Update(entity);
            MessageBox.Show("Сущность успешно обновлена!", "Info.", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        catch (Exception ex)
        {
            MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }

    /// <summary>
    /// Удаление сущности.
    /// </summary>
    /// <param name="entity"></param>
    public void DeleteEntity(T entity)
    {
        try
        {
            DbFacade.Remove<T>(entity);
        }
        catch (Exception ex)
        {
            MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}