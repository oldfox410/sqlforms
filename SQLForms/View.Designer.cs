﻿namespace SQLForms;

partial class View
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.dataView = new System.Windows.Forms.ListBox();
            this.tableSelect = new System.Windows.Forms.ComboBox();
            this.refresh = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dataView
            // 
            this.dataView.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Top;
            this.dataView.FormattingEnabled = true;
            this.dataView.ItemHeight = 20;
            this.dataView.Location = new System.Drawing.Point(11, 51);
            this.dataView.Name = "dataView";
            this.dataView.Size = new System.Drawing.Size(777, 384);
            this.dataView.TabIndex = 0;
            this.dataView.MouseDoubleClick += new MouseEventHandler(this.DataViewDoubleClick);
            // 
            // tableSelect
            // 
            this.tableSelect.FormattingEnabled = true;
            this.tableSelect.Location = new System.Drawing.Point(11, 12);
            this.tableSelect.Name = "tableSelect";
            this.tableSelect.Size = new System.Drawing.Size(151, 28);
            this.tableSelect.TabIndex = 1;
            this.tableSelect.SelectedIndexChanged += new System.EventHandler(this.TableSelectSelectedIndexChanged);
            // 
            // refresh
            // 
            this.refresh.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            this.refresh.Location = new System.Drawing.Point(641, 9);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(147, 31);
            this.refresh.TabIndex = 2;
            this.refresh.Text = "Обновить";
            this.refresh.UseVisualStyleBackColor = true;
            this.refresh.Click += new System.EventHandler(this.RefreshClick);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 451);
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.tableSelect);
            this.Controls.Add(this.dataView);
            this.Name = "View";
            this.Text = "View";
            this.ResumeLayout(false);

    }

    #endregion

    private ListBox dataView;
    private ComboBox tableSelect;
    private Button refresh;
}