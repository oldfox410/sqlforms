﻿namespace SQLForms;

partial class ExamForm
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.add = new System.Windows.Forms.Button();
        preview = new Button();
        this.studentId = new System.Windows.Forms.ComboBox();
        this.label4 = new System.Windows.Forms.Label();
        this.grade = new System.Windows.Forms.TextBox();
        this.label1 = new System.Windows.Forms.Label();
        this.examDate = new System.Windows.Forms.DateTimePicker();
        this.label3 = new System.Windows.Forms.Label();
        this.SuspendLayout();
        // 
        // add
        // 
        this.add.Location = new System.Drawing.Point(13, 129);
        this.add.Name = "add";
        this.add.Size = new System.Drawing.Size(184, 29);
        this.add.TabIndex = 0;
        this.add.Text = "Применть";
        this.add.UseVisualStyleBackColor = true;
        this.add.Click += new System.EventHandler(this.OnAddClick);
        //
        // preview
        //
        this.preview.Location = new System.Drawing.Point(202, 129);
        this.preview.Name = "preview";
        this.preview.Size = new System.Drawing.Size(184, 29);
        this.preview.TabIndex = 0;
        this.preview.Text = "Предпросмотр";
        this.preview.UseVisualStyleBackColor = true;
        this.preview.Click += new System.EventHandler(this.OnPreviewClick);
        // 
        // studentId
        // 
        this.studentId.FormattingEnabled = true;
        this.studentId.Location = new System.Drawing.Point(202, 32);
        this.studentId.Name = "studentId";
        this.studentId.Size = new System.Drawing.Size(184, 28);
        this.studentId.TabIndex = 42;
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Location = new System.Drawing.Point(202, 9);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(65, 20);
        this.label4.TabIndex = 41;
        this.label4.Text = "Студент:";
        // 
        // grade
        // 
        this.grade.Location = new System.Drawing.Point(12, 32);
        this.grade.Name = "grade";
        this.grade.Size = new System.Drawing.Size(184, 27);
        this.grade.TabIndex = 40;
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Location = new System.Drawing.Point(12, 9);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(64, 20);
        this.label1.TabIndex = 39;
        this.label1.Text = "Оценка:";
        // 
        // examDate
        // 
        this.examDate.Location = new System.Drawing.Point(12, 85);
        this.examDate.Name = "examDate";
        this.examDate.Size = new System.Drawing.Size(184, 27);
        this.examDate.TabIndex = 44;
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Location = new System.Drawing.Point(12, 62);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(88, 20);
        this.label3.TabIndex = 43;
        this.label3.Text = "Дата сдачи:";
        // 
        // ExamForm
        // 
        this.FormBorderStyle = FormBorderStyle.FixedSingle;
        this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(399, 168);
        this.Controls.Add(this.examDate);
        this.Controls.Add(this.label3);
        this.Controls.Add(this.studentId);
        this.Controls.Add(this.label4);
        this.Controls.Add(this.grade);
        this.Controls.Add(this.label1);
        this.Controls.Add(this.preview);
        this.Controls.Add(this.add);
        this.Name = "ExamForm";
        this.Text = "ExamForm";
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private Button add;
    private Button preview;
    private ComboBox studentId;
    private Label label4;
    private TextBox grade;
    private Label label1;
    private DateTimePicker examDate;
    private Label label3;
}