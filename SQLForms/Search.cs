﻿using SQLForms.DataBase;
using SQLForms.Domain.Contract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLForms;
public partial class Search : Form
{
    public Search()
    {
        InitializeComponent();
    }

    private void searchButton_Click(object sender, EventArgs e)
    {
        dataSearch.Items.Clear();

        List<string> restrictions = new List<string>();

        if (isGroup.Checked)
        {
            restrictions.Add("GroupName = '" + groupFilter.Text + "'");
        }
        if (isFullName.Checked)
        {
            restrictions.Add("FullName LIKE '%" + fullNameFilter.Text + "%'");
        }

        string where = restrictions.Any() ? "WHERE " + restrictions.Aggregate((acc, curr) => acc + " AND " + curr) : "";
        string query = "SELECT * FROM Student " + where;

        var filtered = DbFacade.Read<Student>(query);

        foreach (var item in filtered)
        {
            dataSearch.Items.Add(item);
        }
    }
}
