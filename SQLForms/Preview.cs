﻿using SQLForms.DataBase;
using SQLForms.Domain.Interfaces;

namespace SQLForms;

public partial class Preview<T> : Form
    where T : IEntity, new()
{
    public Preview(T entity)
    {
        Entity = entity;
        InitializeComponent();
        InitTableSelectComponents();
    }

    public int? EntityId => Entity.Id;

    public T Entity { get; set; }

    public void InitTableSelectComponents()
    {
        foreach (var item in DbFacade.Read<T>())
        {
            if (item.Id != EntityId)
                dataView.Items.Add(item);
            else
                dataView.Items.Add(Entity);
        }
        if (EntityId == null)
            dataView.Items.Add(Entity);
    }
}
