﻿namespace SQLForms.Interfaces;

public interface IEntityForm
{
    public int? EntityId { get; set; }

    public void ViewEntity();
}