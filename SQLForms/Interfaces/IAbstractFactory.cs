﻿using SQLForms.Domain.Interfaces;
using SQLForms.Framework;

namespace SQLForms.Interfaces;

public interface IAbstractFactory
{
    public bool CanCreate(Type entityType);

    public IEntityForm? CreateForm();

    public IEntity CreateEntity();

    public IEnumerable<IEntity> GetEntities(string? query = null);

    public static IEnumerable<IAbstractFactory> Factories = new List<IAbstractFactory>()
    {
        new StudentFactory(),
        new ExamFactory(),
        new SpecialityFactory()
    };

    public static IAbstractFactory? GetFactory<T>()
        where T : IEntity, new()
    {
        return GetFactory(typeof(T));
    }

    public static IAbstractFactory? GetFactory(Type entityType)
    {
        return Factories.FirstOrDefault(q => q.CanCreate(entityType));
    }
}
