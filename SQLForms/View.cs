﻿using SQLForms.Domain.Contract;
using SQLForms.Interfaces;

namespace SQLForms;

public partial class View : Form
{
    readonly IAbstractFactory[] _factories = IAbstractFactory.Factories.ToArray();

    IAbstractFactory _currentFactory => _factories[tableSelect.SelectedIndex];

    public View()
    {
        InitializeComponent();
        InitTableSelectComponents();
        tableSelect.SelectedIndex = 0;
    }

    /// <summary>
    /// Заполняем список существующих таблиц.
    /// </summary>
    public void InitTableSelectComponents()
    {
        var items = tableSelect.Items;
        foreach (var entityType in _factories.Select(q => q.CreateEntity().GetType()))
        {
            items.Add(entityType.Name);
        }
    }

    /// <summary>
    /// Отображение данных из DB.
    /// </summary>
    public void ViewData()
    {
        dataView.Items.Clear();
        dynamic? items = null;
        items = _currentFactory.GetEntities();

        if (items == null)
            return;

        foreach (var item in items)
        {
            dataView.Items.Add(item);
        }
    }

    /// <summary>
    /// Обновляем данные на таблице при смене выбранной таблицы.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void TableSelectSelectedIndexChanged(object sender, EventArgs e)
    {
        ViewData();
    }

    /// <summary>
    /// Отображаем меню изменения сущности после двойного щелчка по строчке.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void DataViewDoubleClick(object sender, EventArgs e)
    {
        ViewUpdateDialog();
    }

    /// <summary>
    /// Отображение диалогового окна для изменения сущности.
    /// </summary>
    public void ViewUpdateDialog()
    {

        BaseEntity? entity = (BaseEntity?)dataView.SelectedItem;

        if (entity == null)
            return;

        var factory = IAbstractFactory.GetFactory(entity.GetType());

        if (factory == null)
            return;

        var entityForm = factory.CreateForm();

        if (entityForm == null)
            return;

        entityForm.EntityId = entity.Id;
        entityForm.ViewEntity();
        var form = entityForm as Form;

        if (form == null)
            return;

        form.Text += " (изменение)";

        // Обновляем данные после их изменения в диалоговом окне.
        form.FormClosed += (s, e) => ViewData();

        form.ShowDialog();
    }

    /// <summary>
    /// Обновляем данные в интерфейсе.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void RefreshClick(object sender, EventArgs e)
    {
        ViewData();
    }
}