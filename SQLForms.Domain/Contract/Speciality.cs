﻿namespace SQLForms.Domain.Contract;

public class Speciality : BaseEntity
{
    public string? Name { get; set; }

    public override string ToString()
    {
        return $"Id: {Id}, Name: {Name}";
    }
}
