﻿using SQLForms.Domain.Interfaces;

namespace SQLForms.Domain.Contract;

public abstract class BaseEntity : IEntity
{
    public int? Id { get; set; }
}
