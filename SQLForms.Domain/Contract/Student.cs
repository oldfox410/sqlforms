﻿namespace SQLForms.Domain.Contract;

public class Student : BaseEntity
{
    public string? FullName { get; set; }

    public int RecordBookNumber { get; set; }

    public DateTime BirthDate { get; set; }

    public int? SpecialityId { get; set; }

    public string? GroupName { get; set; }

    public string? City { get; set; }

    public string? Address { get; set; }

    public override string ToString() => 
        $"Id: {Id}, FullName: {FullName}, " +
        $"RecordBookNumber: {RecordBookNumber}, " +
        $"BirthDate: {BirthDate}, " +
        $"SpecialityId: {SpecialityId}, " +
        $"GroupName: {GroupName}, " +
        $"City: {City}, " +
        $"Address: {Address}";
}
