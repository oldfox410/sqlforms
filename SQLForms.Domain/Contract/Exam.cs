﻿namespace SQLForms.Domain.Contract;

public class Exam : BaseEntity
{
    public int? StudentId { get; set; }
    
    public int Grade { get; set; }
    
    public DateTime ExamDate { get; set; }

    public override string ToString()
    {
        return $"Id: {Id}, StudentId: {StudentId}, Grade: {Grade}, ExamDate: {ExamDate}";
    }
}